public class Cuadrado implements Figura{
    private String name;
    private int PositionX, PositionY;
    public Cuadrado(){}
    @Override
    public void setName(String name){
        this.name=name;
    }
    public String getName(){
        return name;
    }
    public void move(int x, int y){
        this.PositionX=x;
        this.PositionY=y;
    }
    public void getPosition(){
        System.out.println("En eje x " + this.PositionX);
        System.out.println("En eje y " + this.PositionY );
    }
    public Figura clonee(){
        Figura myFigura= new Cuadrado();
        myFigura.setName(this.name);
        myFigura.move(this.PositionX, this.PositionY);
        return myFigura;
    }

}
